UUID support for i18n_taxonomy. With this module term-UUID is used instead of
TID for translations.